#!/Users/joaonetto/.venvs/testDNS/bin/python

from argparse import ArgumentParser
from logging import getLogger
from logging import Formatter
from logging import StreamHandler
from logging import FileHandler
from logging import handlers
from sys import stdout
from sys import exit


def show_message_syslog(syslog_level, syslog_ip, syslog_port, syslog_file):
    syslog_handler = getLogger('MyLogger')
    syslog_handler.setLevel(syslog_level)

    syslog_format = Formatter('%(asctime)s [ %(levelname)s ] %(message)s')

    syslog_handler_terminal = StreamHandler(stdout)
    syslog_handler_terminal.setLevel(syslog_level)
    syslog_handler_terminal.setFormatter(syslog_format)
    syslog_handler.addHandler(syslog_handler_terminal)

    if syslog_ip:
        syslog_handler_external = handlers.SysLogHandler(address=(syslog_ip, syslog_port), facility=5)
        syslog_handler_external.setLevel(syslog_level)
        syslog_handler.addHandler(syslog_handler_external)

    if syslog_file:
        syslog_handler_file = FileHandler(syslog_file)
        syslog_handler_file.setFormatter(syslog_format)
        syslog_handler.addHandler(syslog_handler_file)

    return syslog_handler


if __name__ == "__main__":
    parser = ArgumentParser(description="Send samples of Syslog Messages.")
    parser.add_argument("--save",
                        "-s",
                        help="Give the name of Syslog file to be saved.")
    parser.add_argument("--ip",
                        "-ip",
                        help="Inform the IP address of a Syslog Server to send a message.")

    parser.add_argument("--port",
                        "-p",
                        type=int,
                        default=514,
                        help="Inform the UDP/Port of this Syslog Server. Default: 514.")

    parser.add_argument("--level",
                        "-l",
                        default="DEBUG",
                        help="What Syslog Level do you want to be used ?. Default: DEBUG.")

    parser.add_argument("--message",
                        "-m",
                        default="",
                        help="Write your message.")

    try:
        args = parser.parse_args()
        if args.message == '':
            raise Exception(
                "\nPlease Check:\n=============\nUse the option '-m' or '--message' to write a Syslog Message.")
        syslog = show_message_syslog(args.level.upper(), args.ip, args.port, args.save)
        syslog.info(args.message)
    except Exception as Error:
        parser.print_help()
        print(Error)
        exit(1)
